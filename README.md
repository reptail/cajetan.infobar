Continuation of my earlier InfoBar project (https://bitbucket.org/reptail/infobar). 

Starting from scratch using WPF (Windows Presentation Framework), trying to build it as generic and extendible as possible. 

Download: [From Source](https://bitbucket.org/reptail/cajetan.infobar/src/20223b6bec7bc39d0199b4eb18f6e995a4d60e3d/Releases/?at=master)

2014-04-08: Renamed to "Cajetan.Infobar", and code converted to follow the MVVM pattern. Caused loss of old history.