﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;
using Cajetan.Infobar.Models;

namespace Cajetan.Infobar.Converters
{
    public class HexToBrushConverter : IValueConverter
    {
        private static BrushConverter _converter = new BrushConverter();

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var hex = value as string;
            if (hex != null)
                return (SolidColorBrush)_converter.ConvertFromString(hex);
            return Brushes.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
