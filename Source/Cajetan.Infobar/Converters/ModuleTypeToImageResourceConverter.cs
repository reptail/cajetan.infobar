﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using Cajetan.Infobar.Common;

namespace Cajetan.Infobar.Converters
{
    class ModuleTypeToImageResourceConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var size = "16";
            if (parameter != null && parameter is string)
                size = (string)parameter;

            string image = null;
            if (value is ModuleType)
            {
                switch ((ModuleType)value)
                {
                    case ModuleType.BatteryStatus: image = "battery_charging"; break;
                    case ModuleType.MemoryUsage: image = "memory"; break;
                    case ModuleType.NetworkUsage: image = "network"; break;
                    case ModuleType.ProcessorUsage: image = "cpu"; break;
                    case ModuleType.Uptime: image = "uptime"; break;
                }
            }

            if (image != null)
            {
                var resourcePath = string.Format("/Cajetan.Infobar;component/Resources/Images/{1}/{0}_{1}.png", image, size);
                return resourcePath;
            }
            return "/Cajetan.Infobar;component/Resources/Images/empty.png";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
