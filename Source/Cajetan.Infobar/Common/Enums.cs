﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cajetan.Infobar.Common
{
    public enum ModuleType
    {
        Unknown,

        BatteryStatus,
        MemoryUsage,
        NetworkUsage,
        ProcessorUsage,
        Uptime
    }

    public enum NetworkDisplayFormat
    {
        Auto,

        Bytes,
        Kilobytes,
        Megabytes,
        Gigabytes
    }
}
