﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Infobar.ViewModels;
using Cajetan.Infobar.Services;
using Cajetan.Core.AppBar;
using Cajetan.Infobar.Libraries;
using Cajetan.Infobar.Common;
using System.Windows;
using Autofac;

namespace Cajetan.Infobar
{
    public static class ServiceContainer
    {
        private static IContainer _container;

        public static void Initialize()
        {
            ContainerBuilder cb = new ContainerBuilder();

            // Register Misc
            RegisterMisc(cb);

            // Register ViewModels
            RegisterViewModels(cb);

            // Register Services
            RegisterServices(cb);

            // Create container
            _container = cb.Build();
        }

        public static T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        private static void RegisterMisc(ContainerBuilder cb)
        {
            cb.Register((c, p) => Application.Current.MainWindow);
            cb.RegisterType<AppBarController>()
                .As<IAppBarController>()
                .SingleInstance();
            cb.RegisterType<ViewModelLocator>()
                .As<IViewModelLocator>()
                .SingleInstance();
        }

        private static void RegisterViewModels(ContainerBuilder cb)
        {
            // Misc
            cb.RegisterType<MainViewModel>()
                .SingleInstance();

            // Modules
            cb.RegisterType<BatteryStatusViewModel>()
                .SingleInstance();
            cb.RegisterType<MemoryUsageViewModel>()
                .SingleInstance();
            cb.RegisterType<NetworkUsageViewModel>()
                .SingleInstance();
            cb.RegisterType<ProcessorUsageViewModel>()
                .SingleInstance();
            cb.RegisterType<UptimeViewModel>()
                .SingleInstance();

            // Options
            cb.RegisterType<OptionsViewModel>();
            cb.RegisterType<BatteryStatusOptionsViewModel>();
            cb.RegisterType<MemoryUsageOptionsViewModel>();
            cb.RegisterType<NetworkUsageOptionsViewModel>();
            cb.RegisterType<ProcessorUsageOptionsViewModel>();
            cb.RegisterType<UptimeOptionsViewModel>();
        }

        private static void RegisterServices(ContainerBuilder cb)
        {
            cb.RegisterType<CfgSettingsService>()
                .As<ISettingsService>()
                .SingleInstance();
            cb.RegisterType<WindowService>()
                .As<IWindowService>()
                .SingleInstance();
            cb.RegisterType<SystemInfoService>()
                .As<ISystemInfoService>()
                .SingleInstance();
            cb.RegisterType<HelperService>()
                .As<IHelperService>()
                .SingleInstance();
        }

    }
}
