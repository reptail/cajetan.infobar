﻿using System;
using Cajetan.Infobar.ViewModels;
namespace Cajetan.Infobar.Common
{
    public interface IViewModelLocator
    {
        // Misc
        MainViewModel MainViewModel { get; }

        // Modules
        BatteryStatusViewModel BatteryStatusViewModel { get; }
        MemoryUsageViewModel MemoryUsageViewModel { get; }
        NetworkUsageViewModel NetworkUsageViewModel { get; }
        ProcessorUsageViewModel ProcessorUsageViewModel { get; }
        UptimeViewModel UptimeViewModel { get; }        

        // Options
        OptionsViewModel OptionsViewModel { get; }
        BatteryStatusOptionsViewModel BatteryStatusOptionsViewModel { get; }
        MemoryUsageOptionsViewModel MemoryUsageOptionsViewModel { get; }
        NetworkUsageOptionsViewModel NetworkUsageOptionsViewModel { get; }
        ProcessorUsageOptionsViewModel ProcessorUsageOptionsViewModel { get; }
        UptimeOptionsViewModel UptimeOptionsViewModel { get; }
    }
}
