﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.Common
{
    public class ViewModelLocator : IViewModelLocator
    {

        #region Variables

        private static bool? _isDesignMode;

        #endregion


        #region Properties

        public static bool IsDesignMode
        {
            get
            {
                if (_isDesignMode == null)
                {
                    _isDesignMode = (bool)DependencyPropertyDescriptor
                                      .FromProperty(DesignerProperties.IsInDesignModeProperty, typeof(FrameworkElement))
                                      .Metadata.DefaultValue;
                }
                return _isDesignMode.Value;
            }
        }

        #endregion


        #region ViewModels


        #region Misc

        public MainViewModel MainViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new MainViewModel(null, null, null, null, null);

                    vm.BackgroundColor = "#FF3B3B3B";
                    vm.ForegroundColor = "#FFF5F5F5";
                    vm.BorderColor = "#FF5E6F7F";

                    vm.ActiveModules = new ObservableCollection<ModuleViewModelBase>()
                    {
                        UptimeViewModel,
                        ProcessorUsageViewModel, 
                        MemoryUsageViewModel,
                        NetworkUsageViewModel
                    };
                }
                return ServiceContainer.Resolve<MainViewModel>();
            }
        }

        #endregion
        
        #region Modules

        public BatteryStatusViewModel BatteryStatusViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new BatteryStatusViewModel(null, null);
                    vm.Status = "56% (1h 40m)";

                }
                return ServiceContainer.Resolve<BatteryStatusViewModel>();
            }
        }

        public MemoryUsageViewModel MemoryUsageViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new MemoryUsageViewModel(null, null);

                    vm.Usage = "3712MB / 12323MB";
                    var r = new Random();
                    for (int i = 0; i < 100; i++)
                    {
                        vm.Values.Add(50 + r.Next(-48, 5));
                    }
                }
                return ServiceContainer.Resolve<MemoryUsageViewModel>();
            }
        }

        public NetworkUsageViewModel NetworkUsageViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new NetworkUsageViewModel(null, null);

                    vm.Download = "81.2M";
                    vm.Upload = "657.3K";
                }
                return ServiceContainer.Resolve<NetworkUsageViewModel>();
            }
        }

        public ProcessorUsageViewModel ProcessorUsageViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new ProcessorUsageViewModel(null, null);

                    vm.Usage = "7 %";
                    var r = new Random();
                    for (int i = 0; i < 100; i++)
                    {
                        vm.Values.Add(50 + r.Next(-48, 5));
                    }
                }
                return ServiceContainer.Resolve<ProcessorUsageViewModel>();
            }
        }

        public UptimeViewModel UptimeViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new UptimeViewModel(null, null);
                    vm.Uptime = "0d 01:18:15";
                }
                return ServiceContainer.Resolve<UptimeViewModel>();
            }
        }

        #endregion

        #region Options

        public OptionsViewModel OptionsViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new OptionsViewModel(null, null, null);

                    vm.ModuleOptions = new ObservableCollection<ModuleOptionsViewModelBase>();
                    vm.ModuleOptions.Add(BatteryStatusOptionsViewModel);
                    vm.ModuleOptions.Add(MemoryUsageOptionsViewModel);
                    vm.ModuleOptions.Add(NetworkUsageOptionsViewModel);
                    vm.ModuleOptions.Add(ProcessorUsageOptionsViewModel);
                    vm.ModuleOptions.Add(UptimeOptionsViewModel);
                    vm.SelectedModuleOption = vm.ModuleOptions[2];

                    return vm;
                }
                return ServiceContainer.Resolve<OptionsViewModel>();
            }
        }

        public BatteryStatusOptionsViewModel BatteryStatusOptionsViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new BatteryStatusOptionsViewModel(null);

                    return vm;

                }
                return ServiceContainer.Resolve<BatteryStatusOptionsViewModel>();
            }
        }

        public MemoryUsageOptionsViewModel MemoryUsageOptionsViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new MemoryUsageOptionsViewModel(null);

                    return vm;

                }
                return ServiceContainer.Resolve<MemoryUsageOptionsViewModel>();
            }
        }

        public NetworkUsageOptionsViewModel NetworkUsageOptionsViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new NetworkUsageOptionsViewModel(null);

                    vm.DisplayFormats = new Dictionary<string, NetworkDisplayFormat>();
                    vm.DisplayFormats.Add("Auto", NetworkDisplayFormat.Auto);
                    vm.DisplayFormats.Add("Kilobytes", NetworkDisplayFormat.Kilobytes);
                    vm.SelectedDisplayFormat = NetworkDisplayFormat.Kilobytes;

                    return vm;

                }
                return ServiceContainer.Resolve<NetworkUsageOptionsViewModel>();
            }
        }

        public ProcessorUsageOptionsViewModel ProcessorUsageOptionsViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new ProcessorUsageOptionsViewModel(null);

                    return vm;

                }
                return ServiceContainer.Resolve<ProcessorUsageOptionsViewModel>();
            }
        }

        public UptimeOptionsViewModel UptimeOptionsViewModel
        {
            get
            {
                if (IsDesignMode)
                {
                    var vm = new UptimeOptionsViewModel(null);

                    return vm;

                }
                return ServiceContainer.Resolve<UptimeOptionsViewModel>();
            }
        }

        #endregion


        #endregion

    }
}
