﻿using System;
using Cajetan.Core.AppBar;

namespace Cajetan.Infobar.Libraries
{
    public interface IAppBarController
    {
        bool AnyFullscreen();
        AppBarController.ABEdge CurrentEdge { get; set; }
        void DockBottom();
        void DockNone();
        void DockTop();
        void Undock();
    }
}
