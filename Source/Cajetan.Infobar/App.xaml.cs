﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Cajetan.Infobar
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException;
        }

        void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
        {
            var msg = e.Exception.Message;
            var st = e.Exception.StackTrace;

            //Debug.WriteLine("{0}\n{1}", msg, st);
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            ServiceContainer.Initialize();

            base.OnStartup(e);
        }
    }
}
