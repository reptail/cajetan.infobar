﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Infobar.Services;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.ViewModels
{
    public class UptimeOptionsViewModel : ModuleOptionsViewModelBase
    {
        private ISettingsService _settingsService;

        private bool _showDays;


        public override Common.ModuleType ModuleType
        {
            get { return Common.ModuleType.Uptime; }
        }
        
        public bool ShowDays
        {
            get { return _showDays; }
            set { if (_showDays != value) { _showDays = value; RaisePropertyChanged(() => this.ShowDays); } }
        }

        public UptimeOptionsViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;

            DisplayName = "Uptime";
            Description = "Shows the current system uptime.";

            ShowDays = false;
        }
        
        public override void Update()
        {
            if (_settingsService.Contains("Module_Uptime_IsEnabled"))
                IsEnabled = _settingsService.Get<bool>("Module_Uptime_IsEnabled");

            if (_settingsService.Contains("Module_Uptime_SortOrder"))
                SortOrder = _settingsService.Get<int>("Module_Uptime_SortOrder");

            if (_settingsService.Contains("Module_Uptime_ShowText"))
                ShowText = _settingsService.Get<bool>("Module_Uptime_ShowText");

            if (_settingsService.Contains("Module_Uptime_ShowDays"))
                ShowDays = _settingsService.Get<bool>("Module_Uptime_ShowDays");
        }

        public override void Save()
        {
            _settingsService.Set<bool>("Module_Uptime_IsEnabled", IsEnabled);
            _settingsService.Set<int>("Module_Uptime_SortOrder", SortOrder);
            _settingsService.Set<bool>("Module_Uptime_ShowText", ShowText);
            _settingsService.Set<bool>("Module_Uptime_ShowDays", ShowDays);
        }
    }
}
