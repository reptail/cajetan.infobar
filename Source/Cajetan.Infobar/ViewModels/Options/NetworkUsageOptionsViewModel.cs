﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Infobar.Common;
using Cajetan.Infobar.Services;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.ViewModels
{
    public class NetworkUsageOptionsViewModel : ModuleOptionsViewModelBase
    {
        private ISettingsService _settingsService;
        private IDictionary<string, NetworkDisplayFormat> _displayFormats;
        private NetworkDisplayFormat _selectedDisplayFormat;

        public override Common.ModuleType ModuleType
        {
            get { return Common.ModuleType.NetworkUsage; }
        }


        public IDictionary<string, NetworkDisplayFormat> DisplayFormats
        {
            get { return _displayFormats; }
            set { if (_displayFormats != value) { _displayFormats = value; RaisePropertyChanged(() => this.DisplayFormats); } }
        }

        public NetworkDisplayFormat SelectedDisplayFormat
        {
            get { return _selectedDisplayFormat; }
            set { if (_selectedDisplayFormat != value) { _selectedDisplayFormat = value; RaisePropertyChanged(() => this.SelectedDisplayFormat); } }
        }

        public NetworkUsageOptionsViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;

            DisplayName = "Network Usage";
            Description = "Shows the current download and upload.";

            IsEnabled = true;
            SelectedDisplayFormat = NetworkDisplayFormat.Auto;
        }

        public override void Update()
        {
            if (_settingsService.Contains("Module_Network_IsEnabled"))
                IsEnabled = _settingsService.Get<bool>("Module_Network_IsEnabled");

            if (_settingsService.Contains("Module_Network_SortOrder"))
                SortOrder = _settingsService.Get<int>("Module_Network_SortOrder");

            DisplayFormats = new Dictionary<string, NetworkDisplayFormat>();
            DisplayFormats.Add("Auto", NetworkDisplayFormat.Auto);
            DisplayFormats.Add("Bytes (B/s)", NetworkDisplayFormat.Bytes);
            DisplayFormats.Add("Kilobytes (kB/s)", NetworkDisplayFormat.Kilobytes);
            DisplayFormats.Add("Megabytes (MB/s)", NetworkDisplayFormat.Megabytes);
            DisplayFormats.Add("Gigabytes (GB/s)", NetworkDisplayFormat.Gigabytes);

            if (_settingsService.Contains("Module_Network_DisplayFormat"))
                SelectedDisplayFormat = _settingsService.Get<NetworkDisplayFormat>("Module_Network_DisplayFormat");
        }

        public override void Save()
        {
            _settingsService.Set<int>("Module_Network_SortOrder", SortOrder);
            _settingsService.Set<NetworkDisplayFormat>("Module_Network_DisplayFormat", SelectedDisplayFormat);
        }
    }
}
