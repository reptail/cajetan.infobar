﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Infobar.Services;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.ViewModels
{
    public class ProcessorUsageOptionsViewModel : ModuleOptionsViewModelBase
    {
        private ISettingsService _settingsService;

        private bool _showGraph;


        public override Common.ModuleType ModuleType
        {
            get { return Common.ModuleType.ProcessorUsage; }
        }


        public bool ShowGraph
        {
            get { return _showGraph; }
            set { if (_showGraph != value) { _showGraph = value; RaisePropertyChanged(() => this.ShowGraph); } }
        }

        public ProcessorUsageOptionsViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;

            DisplayName = "CPU Usage";
            Description = "Shows the current CPU usage in percent.";

            ShowGraph = true;
        }


        public override void Update()
        {
            if (_settingsService.Contains("Module_Processor_IsEnabled"))
                IsEnabled = _settingsService.Get<bool>("Module_Processor_IsEnabled");

            if (_settingsService.Contains("Module_Processor_SortOrder"))
                SortOrder = _settingsService.Get<int>("Module_Processor_SortOrder");

            if (_settingsService.Contains("Module_Processor_ShowText"))
                ShowText = _settingsService.Get<bool>("Module_Processor_ShowText");

            if (_settingsService.Contains("Module_Processor_ShowGraph"))
                ShowGraph = _settingsService.Get<bool>("Module_Processor_ShowGraph");
        }

        public override void Save()
        {
            _settingsService.Set<bool>("Module_Processor_IsEnabled", IsEnabled);
            _settingsService.Set<int>("Module_Processor_SortOrder", SortOrder);
            _settingsService.Set<bool>("Module_Processor_ShowText", ShowText);
            _settingsService.Set<bool>("Module_Processor_ShowGraph", ShowGraph);
        }
    }
}
