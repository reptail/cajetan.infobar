﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Infobar.Services;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.ViewModels
{
    public class MemoryUsageOptionsViewModel : ModuleOptionsViewModelBase
    {
        private ISettingsService _settingsService;
        private bool _showGraph;


        public override Common.ModuleType ModuleType
        {
            get { return Common.ModuleType.MemoryUsage; }
        }

        public bool ShowGraph
        {
            get { return _showGraph; }
            set { if (_showGraph != value) { _showGraph = value; RaisePropertyChanged(() => this.ShowGraph); } }
        }

        public MemoryUsageOptionsViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;

            DisplayName = "Memory Usage";
            Description = "Shows the the currently used memory and the total installed.";

            ShowGraph = true;
        }

        public override void Update()
        {
            if (_settingsService.Contains("Module_Memory_IsEnabled"))
                IsEnabled = _settingsService.Get<bool>("Module_Memory_IsEnabled");

            if (_settingsService.Contains("Module_Memory_SortOrder"))
                SortOrder = _settingsService.Get<int>("Module_Memory_SortOrder");

            if (_settingsService.Contains("Module_Memory_ShowText"))
                ShowText = _settingsService.Get<bool>("Module_Memory_ShowText");

            if (_settingsService.Contains("Module_Memory_ShowGraph"))
                ShowGraph = _settingsService.Get<bool>("Module_Memory_ShowGraph");
        }

        public override void Save()
        {
            _settingsService.Set<bool>("Module_Memory_IsEnabled", IsEnabled);
            _settingsService.Set<int>("Module_Memory_SortOrder", SortOrder);
            _settingsService.Set<bool>("Module_Memory_ShowText", ShowText);
            _settingsService.Set<bool>("Module_Memory_ShowGraph", ShowGraph);
        }
    }
}
