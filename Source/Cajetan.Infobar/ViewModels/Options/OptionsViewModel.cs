﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Cajetan.Core.Mvvm;
using Cajetan.Infobar.Common;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class OptionsViewModel : ViewModelBase
    {
        #region Enum

        private enum MoveDirection { None, Up, Down }

        #endregion

        #region Variables

        private ISettingsService _settingsService;
        private IWindowService _windowService;
        private IViewModelLocator _viewModelLocator;

        private string _backgroundColor;
        private string _foregroundColor;
        private string _borderColor;

        private int _updateInterval;

        private ObservableCollection<ModuleOptionsViewModelBase> _moduleOptions;
        private ModuleOptionsViewModelBase _selectedModuleOption;

        #endregion

        #region Properties

        public ICommand MoveUpCommand { get { return new RelayCommand(param => this.MoveElement(param as ModuleOptionsViewModelBase, MoveDirection.Up)); } }
        public ICommand MoveDownCommand { get { return new RelayCommand(param => this.MoveElement(param as ModuleOptionsViewModelBase, MoveDirection.Down)); } }

        public ICommand SaveCommand { get { return new RelayCommand(param => this.Save()); } }
        public ICommand ApplyCommand { get { return new RelayCommand(param => this.Apply()); } }
        public ICommand CancelCommand { get { return new RelayCommand(param => this.Cancel()); } }

        public string BackgroundColor
        {
            get { return _backgroundColor; }
            set { if (_backgroundColor != value) { _backgroundColor = value; RaisePropertyChanged(); } }
        }

        public string ForegroundColor
        {
            get { return _foregroundColor; }
            set { if (_foregroundColor != value) { _foregroundColor = value; RaisePropertyChanged(); } }
        }

        public string BorderColor
        {
            get { return _borderColor; }
            set { if (_borderColor != value) { _borderColor = value; RaisePropertyChanged(); } }
        }

        public int UpdateInterval
        {
            get { return _updateInterval; }
            set { if (_updateInterval != value) { _updateInterval = value; RaisePropertyChanged(() => this.UpdateInterval); } }
        }

        public ObservableCollection<ModuleOptionsViewModelBase> ModuleOptions
        {
            get { return _moduleOptions; }
            set { if (_moduleOptions != value) { _moduleOptions = value; RaisePropertyChanged(() => this.ModuleOptions); } }
        }

        public ModuleOptionsViewModelBase SelectedModuleOption
        {
            get { return _selectedModuleOption; }
            set { if (_selectedModuleOption != value) { _selectedModuleOption = value; RaisePropertyChanged(() => this.SelectedModuleOption); } }
        }

        #endregion


        #region Constructor

        public OptionsViewModel(ISettingsService settingsService, IWindowService windowService, IViewModelLocator viewModelLocator)
        {
            DisplayName = "Cajetan Infobar - Options";

            _settingsService = settingsService;
            _windowService = windowService;
            _viewModelLocator = viewModelLocator;

            BackgroundColor = "#FF3B3B3B";
            ForegroundColor = "#FFF5F5F5";
            BorderColor = "#FF5E6F7F";
            UpdateInterval = 500;
        }

        #endregion


        #region Private Methods
        
        private void MoveElement(ModuleOptionsViewModelBase element, MoveDirection direction)
        {
            if (element != null)
            {
                var currentIndex = ModuleOptions.IndexOf(element);
                if (currentIndex != -1)
                {
                    var movedAny = false; 
                    if (direction == MoveDirection.Up && currentIndex != 0)
                    {
                        ModuleOptions.Move(currentIndex, currentIndex - 1);
                        movedAny = true;
                    }
                    else if (direction == MoveDirection.Down && currentIndex != (ModuleOptions.Count - 1))
                    {
                        ModuleOptions.Move(currentIndex, currentIndex + 1);
                        movedAny = true;
                    }

                    if (movedAny)
                    {
                        for (int i = 0; i < ModuleOptions.Count; i++)
                        {
                            ModuleOptions[i].SortOrder = i + 1;
                        }
                    }
                }
            }
        }

        private void Apply()
        {
            // Save any module option changes to settings service
            foreach (var module in ModuleOptions)
            {
                module.Save();
            }

            // Save general settings
            _settingsService.Set<string>("General_BackgroundColor", BackgroundColor);
            _settingsService.Set<string>("General_ForegroundColor", ForegroundColor);
            _settingsService.Set<string>("General_BorderColor", BorderColor);
            _settingsService.Set<int>("General_RefreshInterval_Milliseconds", UpdateInterval);

            // Save changes
            _settingsService.SaveChanges();
        }

        private void Save()
        {
            Apply();

            // Close window
            _windowService.CloseWindow(this, true);
        }

        private void Cancel()
        {
            // Cancel 
            _windowService.CloseWindow(this, false);
        }

        #endregion

        #region Public Methods

        public void Update()
        {
            // Add module options
            var modules = new List<ModuleOptionsViewModelBase>();
            modules.Add(_viewModelLocator.BatteryStatusOptionsViewModel);
            modules.Add(_viewModelLocator.MemoryUsageOptionsViewModel);
            modules.Add(_viewModelLocator.NetworkUsageOptionsViewModel);
            modules.Add(_viewModelLocator.ProcessorUsageOptionsViewModel);
            modules.Add(_viewModelLocator.UptimeOptionsViewModel);
            
            // Update modules
            foreach (var module in modules)
                module.Update();
            
            // Sort modules and select first module
            ModuleOptions = new ObservableCollection<ModuleOptionsViewModelBase>(modules.OrderBy(m => m.SortOrder));
            SelectedModuleOption = ModuleOptions.First();


            // Load general settings
            if (_settingsService.Contains("General_BackgroundColor"))
                BackgroundColor = _settingsService.Get<string>("General_BackgroundColor");

            if (_settingsService.Contains("General_ForegroundColor"))
                ForegroundColor = _settingsService.Get<string>("General_ForegroundColor");

            if (_settingsService.Contains("General_BorderColor"))
                BorderColor = _settingsService.Get<string>("General_BorderColor");

            if (_settingsService.Contains("General_RefreshInterval_Milliseconds"))
                UpdateInterval = _settingsService.Get<int>("General_RefreshInterval_Milliseconds");
        }

        #endregion

    }
}
