﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Infobar.Services;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.ViewModels
{
    public class BatteryStatusOptionsViewModel : ModuleOptionsViewModelBase
    {
        private ISettingsService _settingsService;
        private bool _showTime;

        public override Common.ModuleType ModuleType
        {
            get { return Common.ModuleType.BatteryStatus; }
        }
        
        public bool ShowTime
        {
            get { return _showTime; }
            set { if (_showTime != value) { _showTime = value; RaisePropertyChanged(() => this.ShowTime); } }
        }

        public BatteryStatusOptionsViewModel(ISettingsService settingsService)
        {
            _settingsService = settingsService;

            DisplayName = "Battery Status";
            Description = "Shows the battery charge in percentage.";


            ShowTime = true;
        }

        public override void Update()
        {
            if (_settingsService.Contains("Module_Battery_IsEnabled"))
                IsEnabled = _settingsService.Get<bool>("Module_Battery_IsEnabled");

            if (_settingsService.Contains("Module_Battery_SortOrder"))
                SortOrder = _settingsService.Get<int>("Module_Battery_SortOrder");

            if (_settingsService.Contains("Module_Battery_ShowText"))
                ShowText = _settingsService.Get<bool>("Module_Battery_ShowText");

            if (_settingsService.Contains("Module_Battery_ShowTime"))
                ShowTime = _settingsService.Get<bool>("Module_Battery_ShowTime");
        }

        public override void Save()
        {
            _settingsService.Set<bool>("Module_Battery_IsEnabled", IsEnabled);
            _settingsService.Set<int>("Module_Battery_SortOrder", SortOrder);
            _settingsService.Set<bool>("Module_Battery_ShowText", ShowText);
            _settingsService.Set<bool>("Module_Battery_ShowTime", ShowTime);
        }
    }
}
