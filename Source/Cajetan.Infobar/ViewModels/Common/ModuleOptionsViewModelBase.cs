﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core.Mvvm;
using Cajetan.Infobar.Common;

namespace Cajetan.Infobar.ViewModels
{
    public abstract class ModuleOptionsViewModelBase : ViewModelBase
    {
        #region Variables

        private string _description;
        private bool _isEnabled;
        private bool _showText;

        #endregion


        #region Properties

        public abstract ModuleType ModuleType { get; }

        public string Description
        {
            get { return _description; }
            set { if (_description != value) { _description = value; RaisePropertyChanged(); } }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { if (_isEnabled != value) { _isEnabled = value; RaisePropertyChanged(); } }
        }

        public bool ShowText
        {
            get { return _showText; }
            set { if (_showText != value) { _showText = value; RaisePropertyChanged(); } }
        }

        public int SortOrder { get; set; }

        #endregion


        #region Methods

        public abstract void Update();
        public abstract void Save();

        #endregion
    }
}
