﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Core.Mvvm;
using Cajetan.Infobar.Common;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public abstract class ModuleViewModelBase : ViewModelBase
    {
        #region Variables

        private bool _showText;
        private bool _isEnabled;

        #endregion


        #region properties

        public abstract ModuleType ModuleType { get; }

        public bool ShowText
        {
            get { return _showText; }
            set { if (_showText != value) { _showText = value; RaisePropertyChanged(); } }
        }

        public bool IsEnabled
        {
            get { return _isEnabled; }
            set { if (_isEnabled != value) { _isEnabled = value; RaisePropertyChanged(); } }
        }
        
        public int SortOrder { get; set; }

        #endregion


        #region Methods
        
        public abstract void Update();
        public abstract void RefreshData();

        #endregion

    }
}
