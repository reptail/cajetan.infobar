﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Cajetan.Core;
using Cajetan.Core.AppBar;
using Cajetan.Core.Mvvm;
using Cajetan.Infobar.Common;
using Cajetan.Infobar.Libraries;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        #region Variables

        private ISettingsService _settingsService;
        private IWindowService _windowService;
        private ISystemInfoService _systemInfoService;
        private IViewModelLocator _viewModelLocator;

        private IAppBarController _appBar;
        private DispatcherTimer _timer;

        private string _backgroundColor = "#000";
        private string _foregroundColor = "#FFF";
        private string _borderColor = "#000";

        private IEnumerable<ModuleViewModelBase> _allModules;
        private ObservableCollection<ModuleViewModelBase> _activeModules;

        #endregion


        #region Properties

        public ICommand ResetCommand { get { return new RelayCommand(param => this.Reset()); } }
        public ICommand DockCommand { get { return new RelayCommand(param => this.Dock()); } }
        public ICommand CloseCommand { get { return new RelayCommand(param => this.Close()); } }
        public ICommand OpenSettingsCommand { get { return new RelayCommand(param => this.OpenSettings()); } }

        public ObservableCollection<ModuleViewModelBase> ActiveModules
        {
            get { return _activeModules; }
            set
            {
                if (_activeModules != value)
                {
                    _activeModules = value;
                    RaisePropertyChanged();
                }
            }
        }

        public string BackgroundColor
        {
            get { return _backgroundColor; }
            set
            {
                if (_backgroundColor != value)
                {
                    _backgroundColor = value;
                    RaisePropertyChanged();
                }
            }
        }

        public string ForegroundColor
        {
            get { return _foregroundColor; }
            set
            {
                if (_foregroundColor != value)
                {
                    _foregroundColor = value;
                    RaisePropertyChanged();
                }
            }
        }

        public string BorderColor
        {
            get { return _borderColor; }
            set
            {
                if (_borderColor != value)
                {
                    _borderColor = value;
                    RaisePropertyChanged();
                }
            }
        }

        #endregion

        #region Constructor

        public MainViewModel(IAppBarController appBar, ISettingsService settings, IWindowService windowService,
                             ISystemInfoService systemInfoService, IViewModelLocator viewModelLocator)
        {
            _appBar = appBar;
            _settingsService = settings;
            _windowService = windowService;
            _systemInfoService = systemInfoService;
            _viewModelLocator = viewModelLocator;

            _timer = new DispatcherTimer(DispatcherPriority.SystemIdle);
            _timer.Tick += _timer_Tick;
            _timer.Interval = TimeSpan.FromSeconds(1);

            PopulateAllModules();
        }

        #endregion

        #region Private Methods

        private void _timer_Tick(object sender, EventArgs e)
        {
            // Update system info
            _systemInfoService.UpdateInfo();

            // Refresh modules
            foreach (var m in ActiveModules)
                m.RefreshData();
        }

        private void LoadFromSettings()
        {
            if (_settingsService.Contains("General_BackgroundColor"))
                BackgroundColor = _settingsService.Get<string>("General_BackgroundColor");

            if (_settingsService.Contains("General_ForegroundColor"))
                ForegroundColor = _settingsService.Get<string>("General_ForegroundColor");

            if (_settingsService.Contains("General_BorderColor"))
                BorderColor = _settingsService.Get<string>("General_BorderColor");

            if (_settingsService.Contains("General_RefreshInterval_Milliseconds"))
                _timer.Interval = TimeSpan.FromMilliseconds(_settingsService.Get<int>("General_RefreshInterval_Milliseconds"));


            var modulesToActivate = new List<ModuleViewModelBase>();
            foreach (var item in _allModules)
            {
                // Load module settings
                item.Update();

                // Add to active list if enabled
                if (item.IsEnabled)
                    modulesToActivate.Add(item);
            }
            
            // Add sorted modules
            ActiveModules = new ObservableCollection<ModuleViewModelBase>(modulesToActivate.OrderBy(m => m.SortOrder));
        }

        private void _settingsService_SettingsUpdated(object sender, IEnumerable<string> e)
        {
            LoadFromSettings();
        }

        private void OpenSettings()
        {
            var vm = _viewModelLocator.OptionsViewModel;
            vm.Update();

            _windowService.OpenDialog(vm, false, 600, 620);
        }
        
        private void PopulateAllModules()
        {
            var modules = new List<ModuleViewModelBase>();
            modules.Add(_viewModelLocator.BatteryStatusViewModel);
            modules.Add(_viewModelLocator.MemoryUsageViewModel);
            modules.Add(_viewModelLocator.NetworkUsageViewModel);
            modules.Add(_viewModelLocator.ProcessorUsageViewModel);
            modules.Add(_viewModelLocator.UptimeViewModel);

            _allModules = modules;
        }

        #endregion


        #region Public Methods

        public void Initialize()
        {
            // Load settings
            if (_settingsService != null)
            {
                _settingsService.SettingsUpdated -= _settingsService_SettingsUpdated;
                _settingsService.SettingsUpdated += _settingsService_SettingsUpdated;

                LoadFromSettings();
            }

            // Start update timer
            _timer.Start();
        }


        public void Reset()
        {
            var edge = _appBar.CurrentEdge;
            _appBar.Undock();
            if (edge == AppBarController.ABEdge.Bottom)
                _appBar.DockBottom();
            else if (edge == AppBarController.ABEdge.Top)
                _appBar.DockTop();
            else
                _appBar.DockNone();
        }

        public void Dock()
        {
            _appBar.DockBottom();
        }

        public void Close()
        {
            // Stop timer
            _timer.Stop();

            // Unregister AppBar
            _appBar.Undock();

            // Shutdown application
            Application.Current.Shutdown();
        }

        #endregion
    }
}
