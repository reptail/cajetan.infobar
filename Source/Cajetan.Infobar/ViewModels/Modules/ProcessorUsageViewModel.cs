﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Infobar.Common;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class ProcessorUsageViewModel : ModuleViewModelBase
    {
        private ISettingsService _settingsService;
        private ISystemInfoService _systemInfoService;

        private bool _showGraph;
        private string _usage;
        private readonly ObservableCollection<int> _values = new ObservableCollection<int>();

        public bool ShowGraph
        {
            get { return _showGraph; }
            set { if (_showGraph != value) { _showGraph = value; RaisePropertyChanged(); } }
        }

        public string Usage
        {
            get { return _usage; }
            set { if (_usage != value) { _usage = value; RaisePropertyChanged(); } }
        }

        public ObservableCollection<int> Values { get { return _values; } }

        public override ModuleType ModuleType
        {
            get { return ModuleType.ProcessorUsage; }
        }

        public ProcessorUsageViewModel(ISettingsService settings, ISystemInfoService systemInfo)
        {
            _settingsService = settings;
            _systemInfoService = systemInfo;
        }

        public override void Update()
        {
            if (_settingsService.Contains("Module_Processor_IsEnabled"))
                IsEnabled = _settingsService.Get<bool>("Module_Processor_IsEnabled");

            if (_settingsService.Contains("Module_Processor_SortOrder"))
                SortOrder = _settingsService.Get<int>("Module_Processor_SortOrder");

            if (_settingsService.Contains("Module_Processor_ShowText"))
                ShowText = _settingsService.Get<bool>("Module_Processor_ShowText");

            if (_settingsService.Contains("Module_Processor_ShowGraph"))
                ShowGraph = _settingsService.Get<bool>("Module_Processor_ShowGraph");
        }

        public override void RefreshData()
        {
            Usage = _systemInfoService.ProcessorUsageString;
            Values.Add(_systemInfoService.ProcessorUsage);
        }
    }
}
