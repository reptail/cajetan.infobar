﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Infobar.Common;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class NetworkUsageViewModel : ModuleViewModelBase
    {
        private ISettingsService _settingsService;
        private ISystemInfoService _systemInfoService;

        private NetworkDisplayFormat _displayFormat;

        private string _upload;
        private string _download;

        public string Upload
        {
            get { return _upload; }
            set { if (_upload != value) { _upload = value; RaisePropertyChanged(() => this.Upload); } }
        }

        public string Download
        {
            get { return _download; }
            set { if (_download != value) { _download = value; RaisePropertyChanged(() => this.Download); } }
        }

        public override ModuleType ModuleType
        {
            get { return Common.ModuleType.NetworkUsage; }
        }

        public NetworkUsageViewModel(ISettingsService settings, ISystemInfoService systemInfo)
        {
            _settingsService = settings;
            _systemInfoService = systemInfo;

            _displayFormat = NetworkDisplayFormat.Auto;
        }

        public override void Update()
        {
            if (_settingsService.Contains("Module_Network_IsEnabled"))
                IsEnabled = _settingsService.Get<bool>("Module_Network_IsEnabled");

            if (_settingsService.Contains("Module_Network_SortOrder"))
                SortOrder = _settingsService.Get<int>("Module_Network_SortOrder");

            if (_settingsService.Contains("Module_Network_DisplayFormat"))
                _displayFormat = _settingsService.Get<NetworkDisplayFormat>("Module_Network_DisplayFormat");
        }

        public override void RefreshData()
        {
            Download = _systemInfoService.NetworkDownloadString;
            Upload = _systemInfoService.NetworkUploadString;
        }

    }
}
