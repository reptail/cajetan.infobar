﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Infobar.Common;
using Cajetan.Infobar.Models;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class BatteryStatusViewModel : ModuleViewModelBase
    {
        private ISettingsService _settingsService;
        private ISystemInfoService _systemInfoService;

        private bool _showTime;
        private string _status;

        public bool ShowTime
        {
            get { return _showTime; }
            set { if (_showTime != value) { _showTime = value; RaisePropertyChanged(() => this.ShowTime); } }
        }

        public string Status
        {
            get { return _status; }
            set { if (_status != value) { _status = value; RaisePropertyChanged(() => this.Status); } }
        }

        public override ModuleType ModuleType
        {
            get { return Common.ModuleType.BatteryStatus; }
        }

        public BatteryStatusViewModel(ISettingsService settings, ISystemInfoService systemInfo)
        {
            _settingsService = settings;
            _systemInfoService = systemInfo;

            ShowTime = true;
        }

        public override void Update()
        {
            if (_settingsService.Contains("Module_Battery_IsEnabled"))
                IsEnabled = _settingsService.Get<bool>("Module_Battery_IsEnabled");

            if (_settingsService.Contains("Module_Battery_SortOrder"))
                SortOrder = _settingsService.Get<int>("Module_Battery_SortOrder");

            if (_settingsService.Contains("Module_Battery_ShowText"))
                ShowText = _settingsService.Get<bool>("Module_Battery_ShowText");

            if (_settingsService.Contains("Module_Battery_ShowTime"))
                ShowTime = _settingsService.Get<bool>("Module_Battery_ShowTime");
        }

        public override void RefreshData()
        {
            Status = _systemInfoService.BatteryStatusString;
        }



    }
}
