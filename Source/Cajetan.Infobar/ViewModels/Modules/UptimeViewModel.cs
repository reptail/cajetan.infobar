﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Infobar.Common;
using Cajetan.Infobar.Models;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class UptimeViewModel : ModuleViewModelBase
    {
        private ISettingsService _settingsService;
        private ISystemInfoService _systemInfoService;

        private bool _showDays;
        private string _uptime;

        public bool ShowDays
        {
            get { return _showDays; }
            set { if (_showDays != value) { _showDays = value; RaisePropertyChanged(() => this.ShowDays); } }
        }

        public string Uptime
        {
            get { return _uptime; }
            set { if (_uptime != value) { _uptime = value; RaisePropertyChanged(() => this.Uptime); } }
        }

        public override ModuleType ModuleType
        {
            get { return Common.ModuleType.Uptime; }
        }

        public UptimeViewModel(ISettingsService settings, ISystemInfoService systemInfo)
        {
            _settingsService = settings;
            _systemInfoService = systemInfo;

            ShowDays = false;
        }

        public override void Update()
        {
            if (_settingsService.Contains("Module_Uptime_IsEnabled"))
                IsEnabled = _settingsService.Get<bool>("Module_Uptime_IsEnabled");

            if (_settingsService.Contains("Module_Uptime_SortOrder"))
                SortOrder = _settingsService.Get<int>("Module_Uptime_SortOrder");

            if (_settingsService.Contains("Module_Uptime_ShowText"))
                ShowText = _settingsService.Get<bool>("Module_Uptime_ShowText");

            if (_settingsService.Contains("Module_Uptime_ShowDays"))
                ShowDays = _settingsService.Get<bool>("Module_Uptime_ShowDays");
        }

        public override void RefreshData()
        {
            Uptime = _systemInfoService.UptimeString;
        }
    }
}
