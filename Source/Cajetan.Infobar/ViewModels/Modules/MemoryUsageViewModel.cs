﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Core;
using Cajetan.Infobar.Common;
using Cajetan.Infobar.Services;

namespace Cajetan.Infobar.ViewModels
{
    public class MemoryUsageViewModel : ModuleViewModelBase
    {
        private ISettingsService _settingsService;
        private ISystemInfoService _systemInfoService;

        private bool _showGraph;
        private string _usage;
        private readonly ObservableCollection<int> _values = new ObservableCollection<int>();

        public bool ShowGraph
        {
            get { return _showGraph; }
            set { if (_showGraph != value) { _showGraph = value; RaisePropertyChanged(() => this.ShowGraph); } }
        }

        public string Usage
        {
            get { return _usage; }
            set { if (_usage != value) { _usage = value; RaisePropertyChanged(() => this.Usage); } }
        }

        public override ModuleType ModuleType
        {
            get { return Common.ModuleType.MemoryUsage; }
        }

        public ObservableCollection<int> Values { get { return _values; } }

        public MemoryUsageViewModel(ISettingsService settings, ISystemInfoService systemInfo)
        {
            _settingsService = settings;
            _systemInfoService = systemInfo;

            ShowGraph = true;
        }

        public override void Update()
        {
            if (_settingsService.Contains("Module_Memory_IsEnabled"))
                IsEnabled = _settingsService.Get<bool>("Module_Memory_IsEnabled");

            if (_settingsService.Contains("Module_Memory_SortOrder"))
                SortOrder = _settingsService.Get<int>("Module_Memory_SortOrder");

            if (_settingsService.Contains("Module_Memory_ShowText"))
                ShowText = _settingsService.Get<bool>("Module_Memory_ShowText");

            if (_settingsService.Contains("Module_Memory_ShowGraph"))
                ShowGraph = _settingsService.Get<bool>("Module_Memory_ShowGraph");
        }

        public override void RefreshData()
        {
            Usage = _systemInfoService.MemoryUsageString;
            Values.Add(_systemInfoService.MemoryUsedPercentage);
        }

    }
}
