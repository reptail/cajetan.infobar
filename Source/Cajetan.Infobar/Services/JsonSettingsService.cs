﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Reflection;
//using System.Text;
//using System.Threading.Tasks;
//using System.Windows;
//using Cajetan.Core;
//using Cajetan.Infobar.Common;
//using Cajetan.Infobar.Models;
//using Cajetan.Infobar.ViewModels;
//using Newtonsoft.Json;
//using Newtonsoft.Json.Converters;
//using Newtonsoft.Json.Linq;


//namespace Cajetan.Infobar.Services
//{
//    public class JsonSettingsService : ISettingsService
//    {
//        #region Variables

//        private readonly JsonSerializerSettings _jsonSettings = new JsonSerializerSettings
//        {
//            Converters = new List<JsonConverter>() { new StringEnumConverter() },
//            DateFormatHandling = DateFormatHandling.IsoDateFormat,
//            Formatting = Formatting.Indented,
//            //TypeNameAssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple,
//            TypeNameHandling = TypeNameHandling.Auto,
//        };

//        private IHelperService _helperService;
//        private readonly string _settingsFile = "Settings.config";
//        private IDictionary<string, ISettingsModel> _settings;
        
//        public event EventHandler<IEnumerable<string>> SettingsUpdated;

//        #endregion


//        #region Properties


//        #endregion


//        #region Constructor

//        public JsonSettingsService(IHelperService helperService)
//        {
//            _helperService = helperService;

//            // Load from disk
//            Initialize();
//        }

//        #endregion


//        #region Private Methods

//        private void Initialize()
//        {
//            // Create new settings container
//            _settings = new Dictionary<string, ISettingsModel>();

//            // Load from disk
//            LoadFromDisk();

//            // Set default values, if none exists
//            SupplimentWithDefaults();

//            // Debug, Save to disk after init with default values
//            //SaveToDisk();
//        }

//        private void SupplimentWithDefaults()
//        {
//            // General
//            SetDefault<string>("General_BackgroundColor", "#FF3B3B3B");
//            SetDefault<string>("General_ForegroundColor", "#FFF5F5F5");
//            SetDefault<string>("General_BorderColor", "#FF5E6F7F");
//            SetDefault<int>("General_RefreshInterval_Milliseconds", 500);

//            // Modules
//            SetDefault<bool>("Module_Battery_IsEnabled", false);
//            SetDefault<int>("Module_Battery_SortOrder", 2);
//            SetDefault<bool>("Module_Battery_ShowText", true);
//            SetDefault<bool>("Module_Battery_ShowTime", true);

//            SetDefault<bool>("Module_Memory_IsEnabled", true);
//            SetDefault<int>("Module_Memory_SortOrder", 4);
//            SetDefault<bool>("Module_Memory_ShowText", true);
//            SetDefault<bool>("Module_Memory_ShowGraph", true);

//            SetDefault<bool>("Module_Network_IsEnabled", true);
//            SetDefault<int>("Module_Network_SortOrder", 5);
//            SetDefault<NetworkDisplayFormat>("Module_Network_DisplayFormat", NetworkDisplayFormat.Auto);

//            SetDefault<bool>("Module_Processor_IsEnabled", true);
//            SetDefault<int>("Module_Processor_SortOrder", 3);
//            SetDefault<bool>("Module_Processor_ShowText", true);
//            SetDefault<bool>("Module_Processor_ShowGraph", true);

//            SetDefault<bool>("Module_Uptime_IsEnabled", true);
//            SetDefault<int>("Module_Uptime_SortOrder", 1);
//            SetDefault<bool>("Module_Uptime_ShowText", true);
//            SetDefault<bool>("Module_Uptime_ShowDays", false);
//        }

//        private void SetDefault<T>(string key, T value)
//        {
//            if (!Contains(key))
//                Set<T>(key, value);
//        }

//        private void SaveToDisk()
//        {
//            try
//            {
//                var data = _settings.Select(s => s.Value).ToArray();
//                string json = JsonConvert.SerializeObject(data, _jsonSettings);
//                File.WriteAllText(_settingsFile, json);
//            }
//            catch (Exception)
//            {
//            }
//        }

//        private void LoadFromDisk()
//        {
//            try
//            {
//                if (File.Exists(_settingsFile))
//                {
//                    var json = File.ReadAllText(_settingsFile);
//                    var data = JsonConvert.DeserializeObject<ISettingsModel[]>(json, _jsonSettings);

//                    foreach (var item in data)
//                    {
//                        if (item.Value.GetType() == typeof(long))
//                            item.Value = Convert.ToInt32(item.Value);

//                        Set(item.Key, item.Value);
//                    }
//                }
//            }
//            catch (Exception)
//            {
//            }
//        }
        
//        #endregion


//        #region Public Methods

//        public bool Contains(string key)
//        {
//            return _settings.ContainsKey(key);
//        }

//        public T Get<T>(string key)
//        {
//            if (Contains(key))
//            {
//                if (typeof(T).IsEnum)
//                    return (T)Enum.Parse(typeof(T), _settings[key].Value.ToString());

//                return (T)_settings[key].Value;
//            }
//            return default(T);
//        }

//        public void Set<T>(string key, T value)
//        {
//            if (Contains(key))
//                _settings[key].Value = value;
//            else
//                _settings[key] = new SettingsModel<T>(key, value);
//        }

//        public void SaveChanges()
//        {
//            try
//            {
//                // Save to disk
//                SaveToDisk();

//                // Raise event
//                RaiseSettingsUpdated(null);
//            }
//            catch (Exception ex)
//            {
//                throw new Exception("Unable to save changes to disk!", ex);
//            }
//        }
        
//        public void RaiseSettingsUpdated(IEnumerable<string> updatedKeys)
//        {
//            if (updatedKeys == null || !updatedKeys.Any())
//                updatedKeys = _settings.Select(s => s.Key).ToList();

//            if (SettingsUpdated != null)
//                SettingsUpdated(this, updatedKeys);
//        }

//        #endregion


//    }
//}
