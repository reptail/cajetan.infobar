﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cajetan.Infobar.Models;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.Services
{
    public interface ISettingsService
    {
        bool Contains(string key);
        T Get<T>(string key);
        void Set<T>(string key, T value);

        void SaveChanges();

        event EventHandler<IEnumerable<string>> SettingsUpdated;
        void RaiseSettingsUpdated(IEnumerable<string> updatedKeys);
    }
}
