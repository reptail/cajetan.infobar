﻿using System;
using Cajetan.Core.Mvvm;

namespace Cajetan.Infobar.Services
{
    public interface IWindowService
    {
        bool Alert(string title, string message);

        bool? OpenDialog(ViewModelBase viewModel);
        bool? OpenDialog(ViewModelBase viewModel, bool allowResize);
        bool? OpenDialog(ViewModelBase viewModel, bool allowResize, double? width, double? height);

        void OpenWindow(ViewModelBase viewModel);
        void OpenWindow(ViewModelBase viewModel, bool allowResize);
        void OpenWindow(ViewModelBase viewModel, bool allowResize, double? width, double? height);

        void CloseWindow(ViewModelBase viewModel);
        void CloseWindow(ViewModelBase viewModel, bool result);
    }
}
