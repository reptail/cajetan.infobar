﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Cajetan.Infobar.Services
{
    public interface IHelperService
    {
        SolidColorBrush HexToBrush(string hex);
        string BrushToHex(Brush brush);
    }
}
