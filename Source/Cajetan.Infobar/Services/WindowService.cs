﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Cajetan.Core.Mvvm;
using Cajetan.Infobar.Models;

namespace Cajetan.Infobar.Services
{    
    public class WindowService : IWindowService
    {

        #region . Variables & Constructor

        private IHelperService _helperService;

        private List<Window> _openWindows;

        public WindowService(IHelperService helperService)
        {
            _helperService = helperService;

            _openWindows = new List<Window>();
        }

        #endregion


        #region . Open Window .

        public void OpenWindow(ViewModelBase viewModel)
        {
            OpenWindow(viewModel, true);
        }

        public void OpenWindow(ViewModelBase viewModel, bool allowResize)
        {
            OpenWindow(viewModel, allowResize, null, null);
        }

        public void OpenWindow(ViewModelBase viewModel, bool allowResize, double? width, double? height)
        {
            var window = CreateWindow(viewModel, allowResize, width, height);

            window.Show();
        }

        #endregion


        #region . Open Dialog .

        public bool? OpenDialog(ViewModelBase viewModel)
        {
            return OpenDialog(viewModel, true);
        }

        public bool? OpenDialog(ViewModelBase viewModel, bool allowResize)
        {
            return OpenDialog(viewModel, allowResize, null, null);
        }

        public bool? OpenDialog(ViewModelBase viewModel, bool allowResize, double? width, double? height)
        {
            var window = CreateWindow(viewModel, allowResize, width, height);

            // Do not show dialog in taskbar
            window.ShowInTaskbar = false;
            // Register KeyDown event
            window.PreviewKeyDown += window_PreviewKeyDown;
            // Show window
            return window.ShowDialog();
        }

        public bool Alert(string title, string message)
        {
            MessageBoxResult result = MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Warning);

            return result == MessageBoxResult.OK;
        }

        #endregion


        #region . Close Window

        public void CloseWindow(ViewModelBase viewModel)
        {
            CloseWindow(viewModel, true);
        }

        public void CloseWindow(ViewModelBase viewModel, bool result)
        {
            var window = _openWindows.FirstOrDefault(w => w.DataContext == viewModel);
            if (window != null)
            {
                //window.Close();
                window.DialogResult = result;
                _openWindows.Remove(window);
            }
        }

        #endregion


        #region . Private Methods .

        private Window CreateWindow(ViewModelBase viewModel, bool allowResize, double? width, double? height)
        {
            var window = new Window();

            try
            {
                window.Owner = Application.Current.MainWindow;
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }
            catch
            {
                window.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }

            window.ResizeMode = allowResize ? ResizeMode.CanResizeWithGrip : ResizeMode.NoResize;
            window.WindowStyle = WindowStyle.SingleBorderWindow;
            window.ShowInTaskbar = true;
            if (width != null && height != null)
            {
                window.SizeToContent = SizeToContent.Manual;
                window.Width = width.Value;
                window.Height = height.Value;
            }
            else
            {
                window.SizeToContent = SizeToContent.WidthAndHeight;
            }

            window.Content = viewModel;
            window.DataContext = viewModel;

            window.ResizeMode = allowResize ? ResizeMode.CanResizeWithGrip : ResizeMode.NoResize;            

            // Bind window title
            if (string.IsNullOrEmpty(window.Title))
                window.SetBinding(Window.TitleProperty, "DisplayName");

            // The WindowManager will always live longer than the window,
            // so we don't need to unsubscribe this event handler.
            window.Closed += window_Closed;
            
            // Add to open windows
            _openWindows.Add(window);

            return window;
        }

        private void window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            var w = sender as Window;
            // Set dialog result if sender is Window and Escape was pressed
            if (w != null && e.Key == Key.Escape)
                w.DialogResult = true;
            // Window will close
        }

        private void window_Closed(object sender, EventArgs e)
        {
            Window window = (Window)sender;

            // Unregister KeyDown event
            window.PreviewKeyDown -= window_PreviewKeyDown;

            // Notify ViewModel of event
            ViewModelBase vm = window.DataContext as ViewModelBase;

            if (vm != null)
            {
                vm.Dispose();
            }

            // Remove from open windows
            _openWindows.Remove(window);
        }

        #endregion

    }
}
