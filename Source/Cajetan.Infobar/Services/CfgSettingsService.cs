﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Cajetan.Infobar.Common;


namespace Cajetan.Infobar.Services
{
    public class CfgSettingsService : ISettingsService
    {
        #region Variables

        private readonly IHelperService _helperService;
        private readonly IWindowService _windowService;
        private readonly string _settingsFile = "Settings.cfg";
        private IDictionary<string, object> _settings;

        public event EventHandler<IEnumerable<string>> SettingsUpdated;

        #endregion


        #region Properties


        #endregion


        #region Constructor

        public CfgSettingsService(IHelperService helperService, IWindowService windowService)
        {
            _helperService = helperService ?? throw new ArgumentNullException(nameof(helperService));
            _windowService = windowService ?? throw new ArgumentNullException(nameof(windowService));

            // Load from disk
            Initialize();
        }

        #endregion


        #region Private Methods

        private void Initialize()
        {
            // Create new settings container
            _settings = new Dictionary<string, object>();

            // Load from disk
            LoadFromDisk();

            // Set default values, if none exists
            SupplimentWithDefaults();

            // Debug, Save to disk after init with default values
            //SaveToDisk();
        }

        private void SupplimentWithDefaults()
        {
            // General
            SetDefault<string>("General_BackgroundColor", "#FF3B3B3B");
            SetDefault<string>("General_ForegroundColor", "#FFF5F5F5");
            SetDefault<string>("General_BorderColor", "#FF5E6F7F");
            SetDefault<int>("General_RefreshInterval_Milliseconds", 500);

            // Modules
            SetDefault<bool>("Module_Battery_IsEnabled", false);
            SetDefault<int>("Module_Battery_SortOrder", 2);
            SetDefault<bool>("Module_Battery_ShowText", true);
            SetDefault<bool>("Module_Battery_ShowTime", true);

            SetDefault<bool>("Module_Memory_IsEnabled", true);
            SetDefault<int>("Module_Memory_SortOrder", 4);
            SetDefault<bool>("Module_Memory_ShowText", true);
            SetDefault<bool>("Module_Memory_ShowGraph", true);

            SetDefault<bool>("Module_Network_IsEnabled", true);
            SetDefault<int>("Module_Network_SortOrder", 5);
            SetDefault<NetworkDisplayFormat>("Module_Network_DisplayFormat", NetworkDisplayFormat.Auto);

            SetDefault<bool>("Module_Processor_IsEnabled", true);
            SetDefault<int>("Module_Processor_SortOrder", 3);
            SetDefault<bool>("Module_Processor_ShowText", true);
            SetDefault<bool>("Module_Processor_ShowGraph", true);

            SetDefault<bool>("Module_Uptime_IsEnabled", true);
            SetDefault<int>("Module_Uptime_SortOrder", 1);
            SetDefault<bool>("Module_Uptime_ShowText", true);
            SetDefault<bool>("Module_Uptime_ShowDays", false);
        }

        private void SetDefault<T>(string key, T value)
        {
            if (!Contains(key))
                Set<T>(key, value);
        }

        private void SaveToDisk()
        {
            try
            {
                var settings = _settings.Select(s => string.Format("{0} {1}", s.Key, s.Value));
                var text = string.Join("\n", settings);
                if (File.Exists(_settingsFile))
                {
                    File.Move(_settingsFile, $"{_settingsFile}.bak");
                }
                File.WriteAllText(_settingsFile, text);
            }
            catch (Exception ex)
            {
                _windowService.Alert("Failed to Save settings!", ex.ToString());
            }
        }

        private void LoadFromDisk()
        {
            try
            {
                if (File.Exists(_settingsFile))
                {
                    var text = File.ReadAllLines(_settingsFile);
                    foreach (var line in text)
                    {
                        var data = line.Split(' ');
                        var key = data[0];
                        var value = data[1];
                        _settings.Add(key, value);
                    }
                }
            }
            catch (Exception ex)
            {
                _windowService.Alert("Failed to Load settings!", ex.ToString());
            }
        }

        #endregion


        #region Public Methods

        public bool Contains(string key)
        {
            return _settings.ContainsKey(key);
        }

        public T Get<T>(string key)
        {
            if (Contains(key))
            {
                if (typeof(T).IsEnum)
                    return (T)Enum.Parse(typeof(T), _settings[key].ToString());

                return (T)Convert.ChangeType(_settings[key], typeof(T));
            }
            return default(T);
        }

        public void Set<T>(string key, T value)
        {
            _settings[key] = value;
        }

        public void SaveChanges()
        {
            try
            {
                // Save to disk
                SaveToDisk();

                // Raise event
                RaiseSettingsUpdated(null);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to save changes to disk!", ex);
            }
        }

        public void RaiseSettingsUpdated(IEnumerable<string> updatedKeys)
        {
            if (updatedKeys == null || !updatedKeys.Any())
                updatedKeys = _settings.Select(s => s.Key).ToList();

            if (SettingsUpdated != null)
                SettingsUpdated(this, updatedKeys);
        }

        #endregion


    }
}
