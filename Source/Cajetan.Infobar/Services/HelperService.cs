﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Cajetan.Infobar.Services
{
    public class HelperService : IHelperService
    {
        private static BrushConverter _converter = new BrushConverter();

        public SolidColorBrush HexToBrush(string hex)
        {
            return (SolidColorBrush)_converter.ConvertFromString(hex);
        }

        public string BrushToHex(Brush brush)
        {
            return _converter.ConvertToString(brush);
        }
    }
}
