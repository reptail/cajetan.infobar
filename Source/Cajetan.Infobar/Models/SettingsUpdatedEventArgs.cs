﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cajetan.Infobar.Models
{
    public class SettingsUpdatedEventArgs : EventArgs
    {
        public IEnumerable<string> UpdatedKeys { get; private set; }

        public SettingsUpdatedEventArgs(IEnumerable<string> updatedKeys)
        {
            UpdatedKeys = updatedKeys;
        }
    }
}
