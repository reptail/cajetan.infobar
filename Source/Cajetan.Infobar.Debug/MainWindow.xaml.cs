﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Cajetan.Infobar.ViewModels;
using AIDA64.SensorValues;

namespace Cajetan.Infobar.Debug
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DebugViewModel _vm;

        public MainWindow()
        {
            Debug();
            Application.Current.Shutdown();
            return;

            InitializeComponent();

            ServiceContainer.Initialize();

            var cpuVM = ServiceContainer.Resolve<ProcessorUsageViewModel>();

            DataContext = _vm = new DebugViewModel(cpuVM);

            _vm.UsagePercentage = 5;            
        }

        public void Debug()
        {
            try
            {
                var values = SharedMemoryValues.GetSensorValues();
                foreach (var v in values)
                {

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FAIL: {0}", ex.Message);
            }
        }
    }
}
