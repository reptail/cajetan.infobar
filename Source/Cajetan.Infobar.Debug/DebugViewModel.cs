﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Threading;
using Cajetan.Core.Mvvm;
using Cajetan.Infobar.ViewModels;

namespace Cajetan.Infobar.Debug
{
    public class DebugViewModel : ViewModelBase
    {
        #region Private Fields

        private DispatcherTimer _timer;
        
        private bool _isStarted;
        private bool _isStopped;
        private int _usagePercentage;


        #endregion


        #region Properties

        public ICommand StartCommand { get { return new RelayCommand(param => this.Start()); } }
        public ICommand StopCommand { get { return new RelayCommand(param => this.Stop()); } }


        public ProcessorUsageViewModel ProcessorUsageViewModel { get; }

        public bool IsStarted
        {
            get { return _isStarted; }
            set { if (_isStarted != value) { _isStarted = value; RaisePropertyChanged(); } }
        }

        public bool IsStopped
        {
            get { return _isStopped; }
            set { if (_isStopped != value) { _isStopped = value; RaisePropertyChanged(); } }
        }

        public int UsagePercentage
        {
            get { return _usagePercentage; }
            set { if (_usagePercentage != value) { _usagePercentage = value; RaisePropertyChanged(); } }
        }

        #endregion

        public DebugViewModel(ProcessorUsageViewModel cpuUsageViewModel)
        {
            ProcessorUsageViewModel = cpuUsageViewModel;
            ProcessorUsageViewModel.Values.Add(0);
            ProcessorUsageViewModel.Values.Add(5);
            ProcessorUsageViewModel.Values.Add(10);
            ProcessorUsageViewModel.Values.Add(25);
            ProcessorUsageViewModel.Values.Add(50);
            ProcessorUsageViewModel.Values.Add(75);
            ProcessorUsageViewModel.Values.Add(100);

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromSeconds(0.25);
            _timer.Tick += (s, e) => ProcessorUsageViewModel.Values.Add(UsagePercentage);

            Start();
        }


        public void Start()
        {
            _timer.Start();
            IsStarted = !(IsStopped = false);

        }

        public void Stop()
        {
            _timer.Start();
            IsStarted = !(IsStopped = true);

        }
    }
}
